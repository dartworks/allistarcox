(function($) {
    $(document).ready(function () {
        var instagramItems = [];
        $.getJSON('library/php/instagram.php', function(result) {
            if((result.meta.code == 200) && (result.data.length > 0)) {
                instagramItems = result.data;
                processInstagramItem();
            }
        });

        function processInstagramItem() {
            var gallery = $('.instagram-gallery .gallery');
            var item = instagramItems.shift();

            var image = $('<img src="'+(item.images.standard_resolution.url)+'" />');
            $(image).data('link', item.link);
            $(image).data('type', item.type);

            $(image).on('load', function() {
                var item = $('<a href="'+$(this).data('link')+'" class="gallery-item" target="_blank" />');
                var outer = $('<div class="outer" />');
                var inner = $('<div class="inner" />');

                if($(this).data('type') == 'image') {
                    var icon = $('<div class="icon"><img src="library/images/instagram-image.svg" /></div>');
                    $(inner).append(icon);
                } else if($(this).data('type') == 'video') {
                    var icon = $('<div class="icon"><img src="library/images/instagram-video.svg" /></div>');
                    $(inner).append(icon);
                }

                $(item).append(outer);
                $(outer).append(inner);

                var src = $(this).attr('src');
                $(inner).css('backgroundImage', 'url('+src+')');
                $(item).css('opacity', 0);

                $(gallery).append(item);
                $(item).fadeTo(1000, 1);

                if(instagramItems.length) {
                    setTimeout(function() {
                        processInstagramItem();
                    }, 200);
                }
            });
        }

        $('.introduction .continue a.arrow').on('click', function(event) {
            event.preventDefault();
            var nextSection = $(this).closest('section').nextAll('section').first();
            if(nextSection.length) {
                $("body, html").stop().animate({
                    scrollTop: nextSection.position().top+'px'
                }, 1000);
            }
        });

        $('.slideshow .close').on('click', function(event) {
            event.preventDefault();

            var slideshow = $('.slideshow');

            $('.overlay').fadeOut(750);
            $(slideshow).animate({
                top: '100%',
                opacity: 0
            }, 750);
        });

        $('.creations article a').on('click', function(event) {
            event.preventDefault();

            var slideshow = $('.slideshow');
            var inner = $(slideshow).find('.clip .inner');
            var images = $(this).find('img');
            var indicatorContainer = $(slideshow).find('.indicator .container');
            if(images.length) {
                $(indicatorContainer).find('a').remove();
                for(var i = 0; i < images.length; ++i) {
                    $(images[i]).data('index', i);

                    var item = $('<a href="" />');
                    if(i == 0) {
                        $(item).addClass('selected');
                    }

                    $(item).on('click', function(event) {
                        event.preventDefault();
                        slideshowChangeSlide($(this).index());
                    });

                    $(indicatorContainer).append(item);
                }

                $(indicatorContainer).removeAttr('style');
                $(slideshow).find('.previous, .next').removeAttr('style');
                if(images.length < 2) {
                    $(indicatorContainer).css('display', 'none');
                    $(slideshow).find('.previous, .next').css('display', 'none');
                }

                $(inner).children().remove();
                var images = $(images).clone(true);
                
                $(inner).append(images);

                $(".clip .inner img").wrap( "<div class='letterbox'></div>");
                $( ".clip .inner img" ).addClass( "slider-img" );

                $(images[0]).parent('div').addClass('selected');

                $('.overlay').fadeIn(750);
                $(slideshow).css('top', '100%');
                $(slideshow).animate({
                    top: '0px',
                    opacity: 1
                }, 750);
            }

            $('.slider-img').each(function(){
                var parentHeight = $(this).parent().height();
                $(this).height(parentHeight);   
            });

        });

        


        var slideshowBusy = false;
        function slideshowChangeSlide(toIndex, direction) {
            var images = $('.slideshow .clip .inner .letterbox');
            var indicators = $('.slideshow .container a');

            if(!$(indicators[toIndex]).hasClass('selected') && !slideshowBusy) {
                slideshowBusy = true;
                
                var fromIndex = $('.slideshow .clip .inner .letterbox.selected').index();
                $(images).removeClass('selected');
                $(indicators).removeClass('selected');
                $(indicators[toIndex]).addClass('selected');

                if(direction === undefined) {
                    if(fromIndex < toIndex) {
                        direction = 'right';
                    } else {
                        direction = 'left';
                    }
                }

                if(direction == 'right') {
                    var fromLeft = '-50%';
                    var toLeft = '100%';
                } else {
                    var fromLeft = '50%';
                    var toLeft = '-100%';
                }

                $(images[fromIndex]).css({
                    position: 'absolute',
                    top: '0px',
                    zIndex: '1001'
                }).animate({
                    left: fromLeft
                }, 1000);

                $(images[toIndex]).css({
                    position: 'absolute',
                    top: '0px',
                    zIndex: '1001',
                    left: toLeft
                }).animate({
                    left: '0px'
                }, 1000, function() {
                    $(images).removeAttr('style');
                    $(images[toIndex]).css({
                        position: 'absolute',
                        top: '0px',
                        left: '0px',
                        //zIndex: 1000
                    });
                    $(images[toIndex]).addClass('selected');

                    slideshowBusy = false;
                });
            }
        }

        $('.slideshow .previous').on('click', function(event) {
            event.preventDefault();

            var indicators = $('.slideshow .container a');
            var fromIndex = $('.slideshow .container a.selected').index();
            var toIndex = fromIndex - 1;
            if(toIndex < 0) {
                toIndex = indicators.length - 1;
            }

            slideshowChangeSlide(toIndex, 'left');
        });

        $('.slideshow .next').on('click', function(event) {
            event.preventDefault();

            var indicators = $('.slideshow .container a');
            var fromIndex = $('.slideshow .container a.selected').index();
            var toIndex = fromIndex + 1;
            if(toIndex > indicators.length - 1) {
                toIndex = 0;
            }

            slideshowChangeSlide(toIndex, 'right');
        });
    });
})(jQuery);
