<!DOCTYPE html>
<html>
    <head>
        <title>Allistar Cox</title>

        <meta name="HandheldFriendly" content="True" />
        <meta name="MobileOptimized" content="320" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="format-detection" content="telephone=no" />

        <link rel="stylesheet" type="text/css" href="library/css/styles.min.css" />
    </head>
    <body>
        <div class="browser-upgrade">
            <h1><span>Allistar</span>Cox</h1>
            <h3>Buildings &amp; Interiors</h3>

            <p>It appears your browser is a little old.<br />Please upgrade to one of the following supported browsers.</p>
            <div class="browsers">
                <a href="https://www.google.com/chrome/" target="_blank" title="Chrome"><img src="library/images/browser-chrome.png" /></a>
                <a href="https://www.mozilla.org/en-US/firefox/new/" target="_blank" title="Firefox"><img src="library/images/browser-firefox.png" /></a>
                <a href="https://www.opera.com/download" target="_blank" title="Opera"><img src="library/images/browser-opera.png" /></a>
                <a href="https://www.microsoft.com/windows/microsoft-edge" target="_blank" title="Edge"><img src="library/images/browser-edge.png" /></a>
            </div>
        </div>
    </body>
</html>