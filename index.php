<?php
$UserAgent = $_SERVER['HTTP_USER_AGENT'];
$Position = strpos($UserAgent, 'MSIE');
if($Position !== false) {
    $UserAgent = trim(substr($UserAgent, $Position + 4));
    $Version = floatval($UserAgent);
    if($Version <= 10.0) {
        include('library/php/browser-upgrade.php');
        exit;
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Allistar Cox</title>

        <meta name="HandheldFriendly" content="True" />
        <meta name="MobileOptimized" content="320" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="format-detection" content="telephone=no" />

        <script src="library/js/jquery-3.3.1.min.js" type="text/javascript" charset="utf-8"></script>
        <script src="library/js/script.js" type="text/javascript" charset="utf-8"></script>
        <link rel="stylesheet" type="text/css" href="library/css/styles.min.css?v=1.1" />
    </head>
    <body>
        <section class="introduction">
            <div class="wrap">
                <div class="logo">
                    <h1><span>Allistar</span>Cox</h1>
                    <h3>Buildings &amp; Interiors</h3>
                </div>
                <div class="continue">
                    <h2>We create shelter;<br />heartening spaces for people to gather and bring to life.</h2>
                    <a href="" class="arrow"></a>
                </div>
            </div>
        </section>

        <div class="section-padding"></div>

        <section class="instagram-gallery">
            <div class="wrap">
                <h4>Instagram Feed - <span class="username"><a href="https://www.instagram.com/allistarcox/" target="_blank">@allistarcox</a></span></h4>
                <div class="gallery"></div>
            </div>
        </section>

        <div class="section-padding"></div>

        <section class="creations">
            <div class="wrap">
                <h4>Creations</h4>
                <article>
                    <a href="">
                        <div class="image">
                            <img src="library/images/placeholders/embla-1.jpg" />
                            <img src="library/images/placeholders/embla-2.jpg" />
                            <img src="library/images/placeholders/embla-3.jpg" />
                            <img src="library/images/placeholders/embla-4.jpg" />
                            <img src="library/images/placeholders/embla-5.jpg" />
                        </div>
                        <h5><span>Embla,</span> Region, Country, Year</h5>
                    </a>
                </article>

                <article>
                    <a href="">
                        <div class="image">
                            <img src="library/images/placeholders/lesa-1.jpg" />
                            <img src="library/images/placeholders/lesa-2.jpg" />
                            <img src="library/images/placeholders/lesa-3.jpg" />
                            <img src="library/images/placeholders/lesa-4.jpg" />
                        </div>
                        <h5><span>Lesa,</span> Region, Country, Year</h5>
                    </a>
                </article>

                <article>
                    <a href="">
                        <div class="image">
                            <img src="library/images/placeholders/love-bucket-1.jpg" />
                            <img src="library/images/placeholders/love-bucket-2.jpg" />
                            <img src="library/images/placeholders/love-bucket-3.jpg" />
                            <img src="library/images/placeholders/love-bucket-4.jpg" />
                            <img src="library/images/placeholders/love-bucket-5.jpg" />
                        </div>
                        <h5><span>Love Bucket,</span> Region, Country, Year</h5>
                    </a>
                </article>

                <article>
                    <a href="">
                        <div class="image">
                            <img src="library/images/placeholders/mojo-1.jpg" />
                            <img src="library/images/placeholders/mojo-2.jpg" />
                            <img src="library/images/placeholders/mojo-3.jpg" />
                        </div>
                        <h5><span>Mojo,</span> Region, Country, Year</h5>
                    </a>
                </article>

                <article>
                    <a href="">
                        <div class="image">
                            <img src="library/images/placeholders/night-jar-1.jpg" />
                            <img src="library/images/placeholders/night-jar-2.jpg" />
                            <img src="library/images/placeholders/night-jar-3.jpg" />
                            <img src="library/images/placeholders/night-jar-4.jpg" />
                        </div>
                        <h5><span>Night Jar,</span> Region, Country, Year</h5>
                    </a>
                </article>

                <article>
                    <a href="">
                        <div class="image">
                            <img src="library/images/placeholders/te-puia-1.jpg" />
                            <img src="library/images/placeholders/te-puia-2.jpg" />
                            <img src="library/images/placeholders/te-puia-3.jpg" />
                            <img src="library/images/placeholders/te-puia-4.jpg" />
                            <img src="library/images/placeholders/te-puia-5.jpg" />
                        </div>
                        <h5><span>Te Puia,</span> Region, Country, Year</h5>
                    </a>
                </article>

                <div class="divider"></div>

                <p>For further work <a href="mailto:allistar@allistarcox.com "><span>email us</span>.</a></p>
            </div>
        </section>

        <div class="section-padding"></div>

        <section class="contact">
            <div class="wrap">
                <h5>+64 4 384 9981, allistar@allistarcox.com<br />Level 1, 193 Victoria Street. PO Box 9715, Wellington, New Zealand.</h5>
            </div>
        </section>

        <footer>
            <div class="wrap">
                <h5>&copy; 2018 <span>Allistar</span>Cox</h5>
            </div>
        </footer>

        <div class="overlay"></div>
        <div class="slideshow">
            <a href="" class="close"></a>
            <a href="" class="previous"></a>
            <div class="clip">
                <div class="inner"></div>
            </div>
            <a href="" class="next"></a>
            <div class="indicator">
                <div class="container"></div>
            </div>
        </div>
    </body>
</html>